/*
*********************************************************************************************************
*
*                                   APPLICATION CONFIGURATION FILE
*                                               ECO32
*                                        GENERIC C COMPILER
*
* Filename      : app_cfg.h
* Version       : V1.00.0
* Programmer(s) : MH
*********************************************************************************************************
*/

#ifndef APP_CFG_H
#define APP_CFG_H

                                                            /* UART Task configuration                                  */
#define APP_CFG_UART_TASK_STACK_SIZE            128
#define APP_CFG_UART_TASK_STACK_LIMIT           100
#define APP_CFG_UART_TASK_PRIO                  5


#endif
