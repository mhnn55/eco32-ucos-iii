/*
*********************************************************************************************************
*
*                                       BSP CONFIGURATION FILE
*                                               ECO32
*                                        GENERIC C COMPILER
*
* Filename      : bsp_cfg.h
* Version       : V1.00.0
* Programmer(s) : MH
*********************************************************************************************************
*/

#ifndef  BSP_CFG_H
#define  BSP_CFG_H


/*
*********************************************************************************************************
*                                      UART ISR CONFIGURATION
*
* Note(s) : (1) Configure BSP_CFG_UARTx_[IN/OUT]_EN to enable or disable the interrupt handling for
*               the uart lines.
*********************************************************************************************************
*/
                                    /* Enable UART lines (see Note #1)                                                          */
#define BSP_CFG_UART0_IN_EN         1
#define BSP_CFG_UART1_IN_EN         0


#endif
