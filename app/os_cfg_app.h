/*
*********************************************************************************************************
*
*                                   OS CONSTANTS CONFIGURATION FILE
*                                               ECO32
*                                        GENERIC C COMPILER
*
* Filename      : os_cfg_app.h
* Version       : V1.00.0
* Programmer(s) : MH
*********************************************************************************************************
*/

#ifndef OS_CFG_APP_H
#define OS_CFG_APP_H

/*
************************************************************************************************************************
*                                                      CONSTANTS
************************************************************************************************************************
*/

                                                            /* --------------------- MISCELLANEOUS --------------------------- */
#define  OS_CFG_MSG_POOL_SIZE            100u               /* Maximum number of messages                                      */
#define  OS_CFG_ISR_STK_SIZE             128u               /* Stack size of ISR stack (number of CPU_STK elements)  min = 128 */
#define  OS_CFG_TASK_STK_LIMIT_PCT_EMPTY  10u               /* Stack limit position in percentage to empty                     */


                                                            /* ---------------------- IDLE TASK ------------------------------ */
#define  OS_CFG_IDLE_TASK_STK_SIZE        128u              /* Stack size (number of CPU_STK elements)               min = 128 */


                                                            /* ------------------ ISR HANDLER TASK --------------------------- */
#define  OS_CFG_INT_Q_SIZE                10u               /* Size of ISR handler task queue                                  */
#define  OS_CFG_INT_Q_TASK_STK_SIZE      128u               /* Stack size (number of CPU_STK elements)               min = 128 */


                                                            /* ------------------- STATISTIC TASK ---------------------------- */
#define  OS_CFG_STAT_TASK_PRIO             6u               /* Priority                                                        */
#define  OS_CFG_STAT_TASK_RATE_HZ         10u               /* Rate of execution (1 to 10 Hz)                                  */
#define  OS_CFG_STAT_TASK_STK_SIZE       128u               /* Stack size (number of CPU_STK elements)               min = 128 */


                                                            /* ------------------------ TICKS -------------------------------- */
#define  OS_CFG_TICK_RATE_HZ             100u               /* Tick rate in Hertz (10 to 1000 Hz)                              */
#define  OS_CFG_TICK_TASK_PRIO             7u               /* Priority                                                        */
#define  OS_CFG_TICK_TASK_STK_SIZE       128u               /* Stack size (number of CPU_STK elements)               min = 128 */
#define  OS_CFG_TICK_WHEEL_SIZE           17u               /* Number of 'spokes' in tick  wheel; SHOULD be prime              */


                                                            /* ----------------------- TIMERS -------------------------------- */
#define  OS_CFG_TMR_TASK_PRIO              8u               /* Priority of 'Timer Task'                                        */
#define  OS_CFG_TMR_TASK_RATE_HZ          10u               /* Rate for timers (10 Hz Typ.)                                    */
#define  OS_CFG_TMR_TASK_STK_SIZE         128u               /* Stack size (number of CPU_STK elements)               min = 128 */
#define  OS_CFG_TMR_WHEEL_SIZE            17u               /* Number of 'spokes' in timer wheel; SHOULD be prime              */

#endif
