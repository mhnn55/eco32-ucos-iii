#include <cpu.h>
#include <lib_def.h>
#include <os.h>
#include <app_cfg.h>
#include <bsp.h>
#include <bsp_uart.h>
#include <bsp_timer.h>


/*
*********************************************************************************************************
*                                              ISR TABLE
*********************************************************************************************************
*/

typedef void (*isr)(void);

const isr isrtbl[32] = {
    RESET,                  // 00: terminal 0 transmitter interrupt
    RESET,                  // 01: terminal 0 receiver interrupt
    RESET,                  // 02: terminal 1 transmitter interrupt
    RESET,                  // 03: terminal 1 receiver interrupt
    RESET,                  // 04: keyboard interrupt
    RESET,                  // 05: unused
    RESET,                  // 06: unused
    RESET,                  // 07: unused
    RESET,                  // 08: disk interrupt
    RESET,                  // 09: unused
    RESET,                  // 10: unused
    RESET,                  // 11: unused
    RESET,                  // 12: unused
    RESET,                  // 13: unused
    RESET,                  // 14: timer 0 interrupt
    BSP_CPU_SysTickHandler, // 15: timer 1 interrupt
    RESET,                  // 16: bus timeout exception
    RESET,                  // 17: illegal instruction exception
    RESET,                  // 18: privileged instruction exception
    RESET,                  // 19: divide instruction exception
    RESET,                  // 20: trap instruction exception
    RESET,                  // 21: TLB miss exception
    RESET,                  // 22: TLB write exception
    RESET,                  // 23: TLB invalid exception
    RESET,                  // 24: illegal address exception
    RESET,                  // 25: privileged address exception
    RESET,                  // 26: unused
    RESET,                  // 27: unused
    RESET,                  // 28: unused
    RESET,                  // 29: unused
    RESET,                  // 30: unused
    RESET,                  // 31: unused
};


/*
*********************************************************************************************************
*                                          UART HANDLING
*********************************************************************************************************
*/

static OS_TCB Uart0InTCB;
static CPU_STK Uart0InSTK[APP_CFG_UART_TASK_STACK_SIZE];

static void  Uart0InTsk  (void* arg)
{
    while (DEF_ON) {
        OS_ERR err = (OS_ERR)0;
        CPU_TS ts = (CPU_TS)0;

        /*pend on flag*/

        /* reset on failure */
        if (err != (OS_ERR)0) {
            RESET();
        }
    }
}


/*
*********************************************************************************************************
*                                          MAIN
*********************************************************************************************************
*/

int main(void)
{
    OS_ERR      err;

    CPU_Init();

    OSInit(&err);                                               /* Initialize "uC/OS-II, The Real-Time Kernel"          */

    BSP_Init();

    OSTaskCreate(&Uart0InTCB,                                   /* Create the Uart0In task */
                 "Uart0InTsk",
                 Uart0InTsk,
                 (void*)0,
                 APP_CFG_UART_TASK_PRIO,
                 &Uart0InSTK[0],
                 APP_CFG_UART_TASK_STACK_LIMIT,
                 APP_CFG_UART_TASK_STACK_SIZE,
                 0,
                 0,
                 (void*)0,
                 (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 &err);

    /* reset on failure */
    if (err != 0) {
        RESET();
    }

    OSStart(&err);                                              /* Start multitasking (i.e. give control to uC/OS-III)   */

    return (1);
}
