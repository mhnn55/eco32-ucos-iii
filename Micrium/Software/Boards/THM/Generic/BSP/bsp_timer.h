/*
*********************************************************************************************************
*
*                                        GENERIC TIMER BSP FILE
*
*                                                ECO32
*                                          GENERIC C COMPILER
*
* Filename      : bsp_timer.h
* Version       : V1.00.0
* Programmer(s) : MH
*
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                               MODULE
*
* Note(s) : (1) This CPU header file is protected from multiple pre-processor inclusion through use of
*               the  CPU module present pre-processor macro definition.
*********************************************************************************************************
*/

#ifndef BSP_TIMER_PRESENT                                     /* See Note #1.                                                 */
#define BSP_TIMER_PRESENT


/*
*********************************************************************************************************
*                                           IOMEM DEFINES
*********************************************************************************************************
*/

#define  TIMER0_BASE        0xF0000000
#define  TIMER0_CTL         (*(CPU_INT32U*)(TIMER0_BASE + 0))
#define  TIMER0_DIV         (*(CPU_INT32U*)(TIMER0_BASE + 4))
#define  TIMER0_CNT         (*(CPU_INT32U*)(TIMER0_BASE + 8))
#define  TIMER0_INTNR       14

#define  TIMER1_BASE        0xF0001000
#define  TIMER1_CTL         (*(CPU_INT32U*)(TIMER1_BASE + 0))
#define  TIMER1_DIV         (*(CPU_INT32U*)(TIMER1_BASE + 4))
#define  TIMER1_CNT         (*(CPU_INT32U*)(TIMER1_BASE + 8))
#define  TIMER1_INTNR       15

#define TIMERHZ             50000000
#define TIMER_IE            0x2
#define TIMER_WR            0x1


/*
*********************************************************************************************************
*                                         FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void  BSP_SysTickInit           (void);
void  BSP_CPU_SysTickHandler    (void);


/*
*********************************************************************************************************
*                                             MODULE END
*
* Note(s) : (1) See 'bsp_time.h  MODULE'.
*********************************************************************************************************
*/

#endif
