/*
*********************************************************************************************************
*
*                                        GENERIC UART BSP FILE
*
*                                                ECO32
*                                          GENERIC C COMPILER
*
* Filename      : bsp_uart.h
* Version       : V1.00.0
* Programmer(s) : MH
*
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                               MODULE
*
* Note(s) : (1) This UART header file is protected from multiple pre-processor inclusion through use of
*               the  UART module present pre-processor macro definition.
*********************************************************************************************************
*/

#ifndef BSP_UART_PRESENT                                     /* See Note #1.                                                 */
#define BSP_UART_PRESENT

#include <os.h>


/*
*********************************************************************************************************
*                                           IOMEM DEFINES
*********************************************************************************************************
*/

#define UART0_BASE          0xF0300000
#define UART0_CTL_IN        (*(CPU_INT32U*)(UART0_BASE + 0))
#define UART0_DTA_IN        (*(CPU_INT32U*)(UART0_BASE + 4))
#define UART0_INTNR_IN      1
#define UART0_CTL_OUT       (*(CPU_INT32U*)(UART0_BASE + 8))
#define UART0_DTA_OUT       (*(CPU_INT32U*)(UART0_BASE + 12))
#define UART0_INTNR_OUT     0

#define UART1_BASE          0xF0301000
#define UART1_CTL_IN        (*(CPU_INT32U*)(UART1_BASE + 0))
#define UART1_DTA_IN        (*(CPU_INT32U*)(UART1_BASE + 4))
#define UART1_INTNR_IN      3
#define UART1_CTL_OUT       (*(CPU_INT32U*)(UART1_BASE + 8))
#define UART1_DTA_OUT       (*(CPU_INT32U*)(UART1_BASE + 12))
#define UART1_INTNR_OUT     2


#define UART_IE             0x2
#define UART_RDY            0x1



/*
*********************************************************************************************************
*                                             MODULE END
*
* Note(s) : (1) See 'bsp_uart.h  MODULE'.
*********************************************************************************************************
*/

#endif
