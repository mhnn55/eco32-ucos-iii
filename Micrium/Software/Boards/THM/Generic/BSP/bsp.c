/*
*********************************************************************************************************
*
*                                          GENERIC BSP FILE
*
*                                                ECO32
*                                          GENERIC C COMPILER
*
* Filename      : bsp.c
* Version       : V1.00.0
* Programmer(s) : MH
*
*********************************************************************************************************
*/

#define   BSP_MODULE
#include <bsp.h>
#include <bsp_timer.h>
#include <cpu.h>



/*
*********************************************************************************************************
*                                          BSP_Init()
*
* Description : Initialize the Board Support Package
*
* Return(s)   : none
*********************************************************************************************************
*/

void  BSP_Init (void)
{
    CPU_SR_ALLOC();


    CPU_INT_DIS();

    BSP_SysTickInit();

    CPU_INT_EN();
}


/*
*********************************************************************************************************
*                                          RESET()
*
* Description : RESET the application
*
* Note(s)     : (1) Can be used as a hardfault recover function that will reboot the application
*
* Return(s)   : none
*********************************************************************************************************
*/
void  reset  (void); /* in entry.s */
void  RESET  (void)
{
    CPU_SR_ALLOC();

    CPU_INT_DIS();

    reset();
}
