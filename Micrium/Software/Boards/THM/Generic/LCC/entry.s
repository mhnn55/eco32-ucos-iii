;********************************************************************************************************
;
;                                          ECO32 SIM BSP FILE
;
;                                                ECO32
;                                         LCC ANSI-C COMPILER
;
; Filename      : entry.s
; Version       : V1.00.0
; Programmer(s) : MH
;********************************************************************************************************


;********************************************************************************************************
;                                           EXTERNAL FUNCTIONS
;********************************************************************************************************

    .import _bbss
    .import _ebss
    .import _initstacktop
    .import main
    .import OSCfg_ISRStkBasePtr
    .import OSCfg_ISRStkSizeRAM
    .import OSTCBCurPtr
    .import BSP_CPU_SysTickHandler
    .import BSP_Uart0_RcvHandler
    .import BSP_Uart1_RcvHandler
    .import OSIntEnter
    .import OSIntExit
    .import psw
    .import isrtbl


;********************************************************************************************************
;                                           EXPORTED FUNCTIONS
;********************************************************************************************************

    .export reset


;********************************************************************************************************
;                                      CODE GENERATION DIRECTIVES
;********************************************************************************************************

    .code
    .align  4

;********************************************************************************************************
;                                            EXCEPTION VECTOR
;
; Description : This is the CPUs exception vector. The CPU jumps here in case of:
;
;               (1) reset
;
;               (2) interrupts/exceptions
;
;               (3) user tlb miss
;
; Note(s)     : (1) This file needs to be linked first since it is the entry of the application.
;                   If not linked first the exception vector will not contain valid jump instrcutions.
;********************************************************************************************************

reset:
    j       startup
exception:
    j       isr
umiss:
    j       isr


;********************************************************************************************************
;                                                 STARTUP
;
; Description : This is the startup of the application. reset jumps here when the CPU starts execution or
;               when to application is loaded to ram and starte.
;
; Note(s)     : (1) The startup invalidades the TLB even uCOS does not use it.
;
; Returns     : Must not return
;********************************************************************************************************

startup:
    ; protection first!
    mvts    $0,0

    ; invalidate TLB
    ; fill with direct-mapped virtual addresses
    mvts    $0,3
    ldhi    $8,0xC0000000
    mvts    $8,2
    add     $8,$0,0
    add     $9,$0,32
tlb_inv_loop:
    mvts    $8,1
    tbwi
    add     $8,$8,1
    bne     $8,$9,tlb_inv_loop

    ; clear BSS
    add     $8,$0,_bbss
    add     $9,$0,_ebss
    j       bss_clr_test
bss_clr_loop:
    stw     $0,$8,0
    add     $8,$8,4
bss_clr_test:
    bltu    $8,$9,bss_clr_loop

    ; setup stack pointer
    add     $29,$0,_initstacktop
    sub     $29,$29,4

    ; update psw
    add     $8,$0,psw
    ldhi    $9,0x08000000
    stw     $9,$8,0
    ; load initial psw
    mvts    $9,0

    ; jump to main
    jal     main
    j       reset


;********************************************************************************************************
;                                               COMMON ISR
;
; Description : This is the common ISR that is executed on all interrupts and exceptions. It needs to
;               perform the following operations:
;
;               (1) Save all register
;
;               (2) Save SP to OSTCBCurPtr->StkPtr
;
;               (3) Switch to ISR stack
;
;               (4) Load and call the ISR
;
;               (5) Switch to tasks stack
;
;               (5) Load all register
;
;               (6) Return to interrupted context
;
; Note(s)     : (1) The register need to be saved and restored in the same way as OSTaskStkInit does save
;                   them or they will not contain valid values after they are restored.
;
; Returns     : None
;********************************************************************************************************

isr:
    .nosyn
    ; save register
    sub     $29,$29,4*32
    stw     $1,$29,4*1
    stw     $2,$29,4*2
    stw     $3,$29,4*3
    stw     $4,$29,4*4
    stw     $5,$29,4*5
    stw     $6,$29,4*6
    stw     $7,$29,4*7
    stw     $8,$29,4*8
    stw     $9,$29,4*9
    stw     $10,$29,4*10
    stw     $11,$29,4*11
    stw     $12,$29,4*12
    stw     $13,$29,4*13
    stw     $14,$29,4*14
    stw     $15,$29,4*15
    stw     $16,$29,4*16
    stw     $17,$29,4*17
    stw     $18,$29,4*18
    stw     $19,$29,4*19
    stw     $20,$29,4*20
    stw     $21,$29,4*21
    stw     $22,$29,4*22
    stw     $23,$29,4*23
    stw     $24,$29,4*24
    stw     $25,$29,4*25
    stw     $26,$29,4*26
    stw     $27,$29,4*27
    stw     $28,$29,4*28
    stw     $29,$29,4*29
    stw     $30,$29,4*30
    stw     $31,$29,4*31
    .syn
    ; update psw
    add     $8,$0,psw
    ldw     $9,$8,0
    add     $10,$0,~(1<<23)
    and     $9,$9,$10
    ldhi    $10,1<<22
    or      $9,$9,$10
    stw     $9,$8,0
    ; OSTCBCurPtr->StkPtr = SP
    add     $8,$0,OSTCBCurPtr
    ldw     $8,$8,0
    stw     $29,$8,0
    ; switch to isr stack
    add     $29,$0,OSCfg_ISRStkBasePtr
    ldw     $29,$29,0
    add     $8,$0,OSCfg_ISRStkSizeRAM
    ldw     $8,$8,0
    add     $29,$29,$8
    jal     OSIntEnter
    ; get exception
    mvfs    $8,0
    slr     $8,$8,16
    and     $8,$8,0x1F
    ; load isr and execute it
    sll     $8,$8,2
    ldw     $8,$8,isrtbl
    jalr    $8
    jal     OSIntExit
    ; return to same task
    ; SP = OSTCBCurPtr->StkPtr
    add     $29,$0,OSTCBCurPtr
    ldw     $29,$29,0
    ldw     $29,$29,0
    ; update psw
    add     $8,$0,psw
    ldw     $9,$8,0
    add     $10,$0,~(1<<22)
    and     $9,$9,$10
    ldhi    $10,1<<23
    or      $9,$9,$10
    stw     $9,$8,0
    ; load regs
    ldw     $1,$29,4*1
    ldw     $2,$29,4*2
    ldw     $3,$29,4*3
    ldw     $4,$29,4*4
    ldw     $5,$29,4*5
    ldw     $6,$29,4*6
    ldw     $7,$29,4*7
    ldw     $8,$29,4*8
    ldw     $9,$29,4*9
    ldw     $10,$29,4*10
    ldw     $11,$29,4*11
    ldw     $12,$29,4*12
    ldw     $13,$29,4*13
    ldw     $14,$29,4*14
    ldw     $15,$29,4*15
    ldw     $16,$29,4*16
    ldw     $17,$29,4*17
    ldw     $18,$29,4*18
    ldw     $19,$29,4*19
    ldw     $20,$29,4*20
    ldw     $21,$29,4*21
    ldw     $22,$29,4*22
    ldw     $23,$29,4*23
    ldw     $24,$29,4*24
    ldw     $25,$29,4*25
    ldw     $26,$29,4*26
    ldw     $27,$29,4*27
    ldw     $28,$29,4*28
    ldw     $30,$29,4*30
    ldw     $31,$29,4*31
    add     $29,$29,4*32
    ; return from exception
    rfx
