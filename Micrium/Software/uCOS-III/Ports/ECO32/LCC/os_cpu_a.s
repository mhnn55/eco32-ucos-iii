;********************************************************************************************************
;
;                                         uCOS-III PORT FILE
;
;                                                ECO32
;                                         LCC ANSI-C COMPILER
;
; Filename      : os_cpu_a.s
; Version       : V1.00.0
; Programmer(s) : MH
;********************************************************************************************************


;********************************************************************************************************
;                                           EXTERNAL FUNCTIONS
;********************************************************************************************************

    .import OSTaskSwHook
    .import OSTCBHighRdyPtr
    .import OSTCBCurPtr
    .import OSPrioHighRdy
    .import OSPrioCur
    .import CPU_SR_Save
    .import psw


;********************************************************************************************************
;                                           PUBLIC FUNCTIONS
;********************************************************************************************************

    .export OSStartHighRdy
    .export OSCtxSw
    .export _OSCtxSw
    .export OSIntCtxSw


;********************************************************************************************************
;                                      CODE GENERATION DIRECTIVES
;********************************************************************************************************

    .code
    .align  4


;********************************************************************************************************
;                                            OSStartHighRdy
;
; Description : This function is called by OSStart() to start multitasking. OSStart will have determined
;               the highest priority task (OSTCBHighRdyPtr will point to the OS_TCB of that task) that
;               was created proper to calling OSStart and will start executing that task.
;
; Prototypes  : void OSStartHighRdy (void);
;
; Note(s)     : (1) Only register 4 and 29 to 31 need to be loaded from stack. Since this is the first
;                   task to start these are the only register containing valid information.
;
;               (2) Interrupts need to be disabled on this context switch if they are enabled early.
;                   After rfx interrupts will be enabled globaly.
;
;               (3) OSStartHighRdy must not be called by the application
;
;               (4) Does not return
;
; Returns      : None
;********************************************************************************************************

OSStartHighRdy:
    ; disable interrupts
    mvfs    $8,0
    add     $9,$0,~(1<<23)
    and     $8,$8,$9
    mvts    $8,0
    ; call hook
    jal     OSTaskSwHook
    ; SP = OSTCBHighRadyPtr->StkPtr
    add     $29,$0,OSTCBHighRdyPtr
    ldw     $29,$29,0
    ldw     $29,$29,0
    ; load valid register
    ldw     $4,$29,4*4
    ldw     $30,$29,4*30
    ldw     $31,$29,4*31
    ; set past interrupt enable
    ldhi    $8,1<<22
    mvfs    $9,0
    or      $9,$9,$8
    mvts    $9,0
    ; update psw
    add     $8,$0,psw
    ldw     $9,$8,0
    ldhi    $10,1<<23
    or      $9,$9,$10
    stw     $9,$8,0
    ; return from "exception" to first task
    rfx


;********************************************************************************************************
;                                                OSCtxSw
;
; Description : This function implements the task level context switch which. It is invoked by the OS_TASK_SW()
;               macro declared in os_cpu.h. It needs to perform the following steps:
;
;               (1) Save callee save and return register
;
;               (2) Save SP to OSCTBCurPtr->StkPtr
;
;               (3) Call OSTaskSwHook
;
;               (4) Update OSCurPrio to OSHighPrio
;
;               (5) Update OSCTCurPtr to OSCTBHighRdyPtr
;
;               (6) Load new SP from OSCTBHighRdyPtr->StkPtr
;
;               (7) Load all register
;
;               (8) Prepare interrupt enable for return
;
;               (9) Return to new context
;
; Prototypes  : void OSCtxSw (void);
;
; Note(s)     : (1) Only callee save register need to be saved since standard abi conventions are
;                   applicable in case of task level switch.
;
;               (2) On restore all register need to be save since we could resume a task that was
;                   preempted after a ISR occured.
;
;               (3) Interrupt enable needs to be prepared before leaving since we could enter a new
;                   task that wont re-enable interrupts after the context switch.
;
; Returns     : none
;********************************************************************************************************
OSCtxSw:
    ; save calle save regs and return address
    sub     $29,$29,4*32
    stw     $16,$29,4*16
    stw     $17,$29,4*17
    stw     $18,$29,4*18
    stw     $19,$29,4*19
    stw     $20,$29,4*20
    stw     $21,$29,4*21
    stw     $22,$29,4*22
    stw     $23,$29,4*23
    stw     $24,$29,4*24
    stw     $25,$29,4*25
    stw     $31,$29,4*30
    ; OSTCBCurPtr->StkPtr = SP
    add     $8,$0,OSTCBCurPtr
    ldw     $8,$8,0
    stw     $29,$8,0
    jal     OSTaskSwHook
    ; OSPrioCur = OSPrioHighRdy
    add     $8,$0,OSPrioHighRdy
    ldb     $8,$8,0
    add     $9,$0,OSPrioCur
    stb     $8,$9,0
    ; OSTCBCurPtr = OSTCBHighRdyPtr
    add     $9,$0,OSTCBHighRdyPtr
    ldw     $9,$9,0
    add     $8,$0,OSTCBCurPtr
    stw     $9,$8,0
    ; SP = OSTCBCurPtr->StkPtr
    add     $29,$0,OSTCBCurPtr
    ldw     $29,$29,0
    ldw     $29,$29,0
    ; update psw
    add     $8,$0,psw
    ldw     $9,$8,0
    ldhi    $10,1<<23
    or      $9,$9,$10
    stw     $9,$8,0
    ; load regs
    ldw     $1,$29,4*1
    ldw     $2,$29,4*2
    ldw     $3,$29,4*3
    ldw     $4,$29,4*4
    ldw     $5,$29,4*5
    ldw     $6,$29,4*6
    ldw     $7,$29,4*7
    ldw     $8,$29,4*8
    ldw     $9,$29,4*9
    ldw     $10,$29,4*10
    ldw     $11,$29,4*11
    ldw     $12,$29,4*12
    ldw     $13,$29,4*13
    ldw     $14,$29,4*14
    ldw     $15,$29,4*15
    ldw     $16,$29,4*16
    ldw     $17,$29,4*17
    ldw     $18,$29,4*18
    ldw     $19,$29,4*19
    ldw     $20,$29,4*20
    ldw     $21,$29,4*21
    ldw     $22,$29,4*22
    ldw     $23,$29,4*23
    ldw     $24,$29,4*24
    ldw     $25,$29,4*25
    ldw     $26,$29,4*26
    ldw     $27,$29,4*27
    ldw     $28,$29,4*28
    ldw     $30,$29,4*30
    ldw     $31,$29,4*31
    add     $29,$29,4*32
    ; make sure interrupts are enabled on return
    .nosyn
    ldhi    $26,1<<22
    mvfs    $27,0
    or      $27,$27,$26
    mvts    $27,0
    .syn
    ; return to new context
    rfx


;********************************************************************************************************
;                                               OSIntCtxSw
;
; Description : This function implements the interrupt level context switch. It is invoked by OSIntExit()
;               If a task switch is allowed and needed. It needs to perform the following operations:
;
;               (1) Call OSTaskSwHook
;
;               (2) Update OSPrio to OSPrioHighRdy
;
;               (3) Update OSCTBCurPtr to OSCTBHighRdyPtr
;
;               (4) Load new SP from OSCTBHighRdyPtr->StkPtr
;
;               (5) Load all register
;
;               (6) Return to new context
;
; Prototypes  : void OSIntCtxSw (void);
;
; Note(s)     : (1) OSINTCtxSw does not need to disable interrupts since it is called from an interrupt
;                   context. interrupts are disabled from within an interrupt context.
;
;               (2) Does not return
;
; Returns     : none
;********************************************************************************************************

OSIntCtxSw:
    jal     OSTaskSwHook
    ; OSPrioCur = OSPrioHighRdy
    add     $8,$0,OSPrioHighRdy
    ldb     $8,$8,0
    add     $9,$0,OSPrioCur
    stb     $8,$9,0
    ; OSTCBCurPtr = OSTCBHighRdyPtr
    add     $9,$0,OSTCBHighRdyPtr
    ldw     $9,$9,0
    add     $8,$0,OSTCBCurPtr
    stw     $9,$8,0
    ; SP = OSTCBCurPtr->StkPtr
    ldw     $29,$9,0
    ; update psw
    add     $8,$0,psw
    ldw     $9,$8,0
    add     $10,$0,~(1<<22)
    and     $9,$9,$10
    ldhi    $10,1<<23
    or      $9,$9,$10
    stw     $9,$8,0
    ; load regs
    ldw     $1,$29,4*1
    ldw     $2,$29,4*2
    ldw     $3,$29,4*3
    ldw     $4,$29,4*4
    ldw     $5,$29,4*5
    ldw     $6,$29,4*6
    ldw     $7,$29,4*7
    ldw     $8,$29,4*8
    ldw     $9,$29,4*9
    ldw     $10,$29,4*10
    ldw     $11,$29,4*11
    ldw     $12,$29,4*12
    ldw     $13,$29,4*13
    ldw     $14,$29,4*14
    ldw     $15,$29,4*15
    ldw     $16,$29,4*16
    ldw     $17,$29,4*17
    ldw     $18,$29,4*18
    ldw     $19,$29,4*19
    ldw     $20,$29,4*20
    ldw     $21,$29,4*21
    ldw     $22,$29,4*22
    ldw     $23,$29,4*23
    ldw     $24,$29,4*24
    ldw     $25,$29,4*25
    ldw     $26,$29,4*26
    ldw     $27,$29,4*27
    ldw     $28,$29,4*28
    ldw     $30,$29,4*30
    ldw     $31,$29,4*31
    add     $29,$29,4*32
    ; return from exception
    rfx
