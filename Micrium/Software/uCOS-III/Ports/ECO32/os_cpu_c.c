/*
*********************************************************************************************************
*
*                                          uCOS-III PORT FILE
*
*                                                ECO32
*                                          GENERIC C COMPILER
*
* Filename      : os_cpu_c.c
* Version       : V1.00.0
* Programmer(s) : MH
*
*********************************************************************************************************
*/

#define   OS_CPU_GLOBALS

#ifdef VSC_INCLUDE_SOURCE_FILE_NAMES
const  CPU_CHAR  *os_cpu_c__c = "$Id: $";
#endif


#include  <os.h>

/*
************************************************************************************************************************
*                                                   IDLE TASK HOOK
*
* Description : This function is called by OSIdleTask(). It is used to call Application Hooks when there is no other high
*               priority task ready.
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : none
************************************************************************************************************************
*/

void  OSIdleTaskHook (void)
{
#if OS_CFG_APP_HOOKS_EN > 0u
    if (OS_AppIdleTaskHookPtr != (OS_APP_HOOK_VOID)0) {
        (*OS_AppIdleTaskHookPtr)();
    }
#endif
}


/*
************************************************************************************************************************
*                                                   INIT HOOK
*
* Description : This function is called by OSInit(). It is used to initilize CPU specific components
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : none
************************************************************************************************************************
*/

void  OSInitHook (void)
{
}


/*
************************************************************************************************************************
*                                                 STAT TASK HOOK
*
* Description : This function is called by OSStatTask(). It is used to track statistics and call Application Hooks when
*               the StatTask is executed.
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : none
************************************************************************************************************************
*/

void  OSStatTaskHook (void)
{
#if OS_CFG_APP_HOOKS_EN > 0u
    if (OS_AppStatTaskHookPtr != (OS_APP_HOOK_VOID)0) {
        (*OS_AppStatTaskHookPtr)();
    }
#endif
}


/*
************************************************************************************************************************
*                                                TASK CREATE HOOK
*
* Description : This function is called by OSTaskCreate() after initialization of OS_TCB fields and setting up the stack
*               frame for the task, just befor adding the task to the ready list. It is used to call Application Hooks
*               when a new task is created.
*
* Arguments   : p_tcb         is a pointer to the TCB of the task being created.
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : none
************************************************************************************************************************
*/

void  OSTaskCreateHook(OS_TCB *p_tcb)
{
#if OS_CFG_APP_HOOKS_EN > 0u
    if (OS_AppTaskCreateHookPtr != (OS_APP_HOOK_TCB)0) {
        (*OS_AppTaskCreateHookPtr)(p_tcb);
    }
#else
    (void)p_tcb;
#endif
}


/*
************************************************************************************************************************
*                                                TASK DELETE HOOK
*
* Description : This function is called by OSTaskDel() after the task is removed from the ready list or any pend list.
*               It is used to call Application Hooks when a task is deleted.
*
* Arguments   : p_tcb         is a pointer to the TCB of the task being deleted.
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : none
************************************************************************************************************************
*/

void OSTaskDelHook(OS_TCB *p_tcb)
{
#if OS_CFG_APP_HOOKS_EN > 0u
    if (OS_AppTaskDelHookPtr != (OS_APP_HOOK_TCB)0) {
        (*OS_AppTaskDelHookPtr)(p_tcb);
    }
#else
    (void)p_tcb;
#endif
}


/*
************************************************************************************************************************
*                                                TASK RETURN HOOK
*
* Description : This function is called by OSTaskReturn() when a user accidently returns from a task. The function needs
*               to delete the task if the Application Hooks do not delete it.
*
* Arguments   : p_tcb         is a pointer to the TCB of the task being returned.
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : Does not return
************************************************************************************************************************
*/

void  OSTaskReturnHook (OS_TCB  *p_tcb)
{
    OS_ERR p_err = (OS_ERR)0;

#if OS_CFG_APP_HOOKS_EN > 0u
    if (OS_AppTaskReturnHookPtr != (OS_APP_HOOK_TCB)0) {
        (*OS_AppTaskReturnHookPtr)(p_tcb);
    }
#endif
#if OS_CFG_TASK_DEL_EN > 0u
    OSTaskDel((OS_TCB*)0, &p_err);
#else
    while(1);
#endif
}


/*
************************************************************************************************************************
*                                            TASK STACK INITIALIZATION
*
* Description : This function is called by OSTaskCreate() to setup the stack fram of the task being created. It must lay
*               the registers on the stack the same way the ISR does it so i can restore them the same way.
*
* Arguments   : p_task        is a pointer to the main function of the task being created
*
*               p_arg         is a pointer to the argument of the task being created
*
*               p_stk_base    is a pointer to the base address of the stack being created
*
*               p_stk_limit   is a pointer to the stack limit watermark of the task being created
*
*               stk_size      is the size of the stack in CPU_STK elements of the task being created
*
*               OS_OPT        is the option of the task being created
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : The new top of the stack after the register are layed out.
************************************************************************************************************************
*/

CPU_STK  *OSTaskStkInit (OS_TASK_PTR    p_task,
                         void          *p_arg,
                         CPU_STK       *p_stk_base,
                         CPU_STK       *p_stk_limit,
                         CPU_STK_SIZE   stk_size,
                         OS_OPT         opt)
{
    CPU_STK *p_stk;

    /* establish new top of stack */
    p_stk = &p_stk_base[stk_size-32-1];

    /* write needed register to stack */
    p_stk[4] = (CPU_STK)p_arg;
    p_stk[30] = (CPU_STK)p_task;
    p_stk[31] = (CPU_STK)OS_TaskReturn;

    return p_stk;
}


/*
************************************************************************************************************************
*                                                TASK SWITCH HOOK
*
* Description : This function is called by OSCtxSw(_OSCtxSw) or OSIntCtxSw before a context switch is performed but after
*               the register of the task are saved on the stack. It is used to perform a various number of operations based
*               on how the system is configured.
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : none
************************************************************************************************************************
*/

void  OSTaskSwHook (void)
{
#if OS_CFG_TASK_PROFILE_EN > 0u
    CPU_TS ts;
#endif
#ifdef CPU_CFG_INT_DIS_MEAS_EN
    CPU_TS int_dis_time;
#endif

#if OS_CFG_APP_HOOKS_EN > 0u
    if (OS_AppTaskSwHookPtr != (OS_APP_HOOK_VOID)0) {
        (*OS_AppTaskSwHookPtr)();
    }
#endif

#if OS_CFG_TASK_PROFILE_EN > 0U
    ts = OS_TS_GET();
    if (OSTCBCurPtr != OSTCBHighRdyPtr) {
        OSTCBCurPtr->CyclesDelta = ts - OSTCBCurPtr->CyclesStart;
        OSTCBCurPtr->CyclesTotal = (OS_CYCLES)OSTCBCurPtr->CyclesDelta;
    }

    OSTCBHighRdyPtr->CyclesStart = ts;
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    int_dis_time = CPU_IntDisMeasMaxCurReset();
    if (OSTCBCurPtr->IntDisTimeMax < int_dis_time) {
        OSTCBCurPtr->IntDisTimeMax = int_dis_time;
    }
#endif

#if OS_CFG_SCHED_LOCK_TIME_MEAS_EN > 0u
    if (OSTCBCurPtr->SchedLockTimeMax < OSSchedLockTimeMaxCur) {
        OSTCBCurPtr->SchedLockTimeMax = OSSchedLockTimeMaxCur;
    }
#endif
}


/*
************************************************************************************************************************
*                                                TIME TICK HOOK
*
* Description : This function is called by OSTimeTick() which is assumed to be called by an ISR. OSTimeTickHook is called
*               at the very begining of OSTimeTick to give priority to the user or port specific code then the tick interrupt
*               occured.
*
* Note(s)     : (1) This function must not be called by the Application
*
* Returns     : none
************************************************************************************************************************
*/

void  OSTimeTickHook (void)
{
#if OS_CFG_APP_HOOKS_EN > 0u
    if (OS_AppTimeTickHookPtr != (OS_APP_HOOK_VOID)0) {
        (*OS_AppTimeTickHookPtr)();
    }
#endif
}
