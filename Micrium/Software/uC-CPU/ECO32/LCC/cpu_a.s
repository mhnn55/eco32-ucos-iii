;********************************************************************************************************
;
;                                            CPU PORT FILE
;
;                                                ECO32
;                                         LCC ANSI-C COMPILER
;
; Filename      : cpu_a.asm
; Version       : V1.00.0
; Programmer(s) : MH
;
;********************************************************************************************************


;********************************************************************************************************
;                                           PUBLIC FUNCTIONS
;********************************************************************************************************

        .export CPU_SR_Save
        .export CPU_SR_Restore
        .export CPU_IntSrc_En
        .export CPU_IntSrc_Di
        .export psw


;********************************************************************************************************
;                                      CODE GENERATION DIRECTIVES
;********************************************************************************************************

        .code
        .align  4


;********************************************************************************************************
;                                      CRITICAL SECTION FUNCTIONS
;
; Description : Disable/Enable interrupts by preserving the state of interrupts.  Generally speaking, the
;               state of the interrupt disable flag is stored in the local variable 'cpu_sr' & interrupts
;               are then disabled ('cpu_sr' is allocated in all functions that need to disable interrupts).
;               The previous interrupt state is restored by copying 'cpu_sr' into the CPU's status register.
;
; Prototypes  : CPU_SR  CPU_SR_Save   (void);
;               void    CPU_SR_Restore(CPU_SR  cpu_sr);
;
; Note(s)     : (1) These functions are used in general like this :
;
;                       void  Task (void  *p_arg)
;                       {
;                           CPU_SR_ALLOC();                     /* Allocate storage for CPU status register */
;                               :
;                               :
;                           CPU_CRITICAL_ENTER();               /* cpu_sr = CPU_SR_Save();                  */
;                               :
;                               :
;                           CPU_CRITICAL_EXIT();                /* CPU_SR_Restore(cpu_sr);                  */
;                               :
;                       }
;********************************************************************************************************

CPU_SR_Save:
    mvts    $0,0
    add     $8,$0,psw
    ldw     $9,$8,0
    add     $10,$0,1<<23
    and     $2,$9,$10
    add     $10,$10,~(1<<23)
    and     $9,$9,$10
    stw     $9,$8,0
    mvts    $9,0
    jr      $31


CPU_SR_Restore:
    mvts    $0,0
    add     $8,$0,psw
    ldw     $9,$8,0
    or      $9,$9,$4
    stw     $9,$8,0
    mvts    $9,0
    jr      $31


;********************************************************************************************************
;                                    INTERRUPT SOURCE ENABLE/DISABLE
;
; Description : Disable and Enable a specific interrupt source
;
; Prototypes  : void  CPU_IntSrc_En (CPU_INT32U intnr)
;               void  CPU_IntSrc_Di (CPU_INT32U intnr)
;********************************************************************************************************

CPU_IntSrc_En:
    add     $8,$0,0xF
    bgtu    $4,$8,out
    mvts    $0,0
    add     $8,$0,psw
    ldw     $9,$8,0
    add     $10,$0,1
    sll     $10,$10,$4
    or      $9,$9,$10
    stw     $9,$8,0
    mvts    $9,0
    jr      $31

CPU_IntSrc_Di:
    add     $8,$0,0xF
    bgtu    $4,$8,out
    mvts    $0,0
    add     $8,$0,psw
    ldw     $9,$8,0
    add     $10,$0,1
    sll     $10,$10,$4
    mul     $10,$10,-1
    and     $9,$9,$10
    stw     $9,$8,0
    mvts    $9,0
out:
    jr      $31


;********************************************************************************************************
;                                      CODE GENERATION DIRECTIVES
;********************************************************************************************************

    .bss
    .align  4


;********************************************************************************************************
;                                           PSW MEMORY MIRROR
;
; Description : A mirror of the psw in memory
;
; Note(s)     : (1) Must be updated whenever the psw is modified in order to keep the nestes critical
;                   regions in takt.
;
;               (2) To update the psw you need to disable interrupts and modify the psw in memory as
;                   needed and then load it to the cpu
;********************************************************************************************************

psw:
    .space 4
